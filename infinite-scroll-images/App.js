import React from 'react';
import { StyleSheet, Image, Text, View, FlatList, Platform, RefreshControl } from 'react-native';
import _ from 'lodash';
import ImageAutoSize from './ImageAutoSize';

function generateImage() {
  return `http://pipsum.com/${_.random(200,600)}x${_.random(200,600)}.jpg`;
}

let i = 0;
export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoadingMore: false,
      imageItems: [],
    };

    this.onEndReached = this.onEndReached.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        imageItems: [
          generateImage(),
          generateImage(),
          generateImage(),
          generateImage(),
          generateImage(),
        ],
      })
    }, 1000);
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          horizontal={false}
          disableVirtualization={true} // important - this ensures that our autosized images don't disrupt scrolling
          scrollEnabled={true}
          style={styles.list}
          data={this.state.imageItems}
          keyExtractor={this.keyExtractor}
          removeClippedSubviews={false}
          renderItem={this.renderItem}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.onEndReached}
          initialNumToRender={5}
          onEndReachedThreshold={0}
          ItemSeparatorComponent={() => <View style={{width: '100%', height: 2, backgroundColor: '#000'}} />}
        />
      </View>
    );
  }

  keyExtractor(item) {
    return item;
  }

  renderItem({item}) {
    return (
      <ImageAutoSize uri={item} shouldClipImage={true} onPressImage={() => {}} />
    )
  }

  renderFooter() {
    if(this.state.isLoadingMore) {
      return (<Text>LOADING MORE!!!!</Text>);
    } else {
      return null;
    }
  }

  onEndReached() {
    console.log('reached end');
    if(this.state.imageItems.length === 0 || this.state.isLoadingMore) {
      console.log('no images yet, or its currently loading');
      return;
    }

    this.setState({
      isLoadingMore: true,
    });

    setTimeout(() => {
      this.setState({
        imageItems: [
          ...this.state.imageItems,
          generateImage(),
          generateImage(),
          generateImage(),
          generateImage(),
          generateImage(),
        ],
        isLoadingMore: false,
      });
    }, 3000);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0F0',
  },
  list: {
    flex: 1,
    backgroundColor: '#F00',
  }
});

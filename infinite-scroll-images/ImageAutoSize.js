import React from 'react';
import { PropTypes } from 'prop-types';
import _ from 'lodash';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

// Clip the height of any images outside these bounds
const MAX_RATIO = 2.0;  // flat images e.g.  2.0 == 375x187.5
const MIN_RATIO = .625; // tall images e.g. .625 == 375x600

/**
 * Given an image uri, this component will present a loading message
 * while the image is fetched followed by displaying a container at the full
 * width of its parent, with the image contained inside at its correct aspect ratio.
 */
class ImageAutoSize extends React.Component {
  constructor(props) {
    super(props);
    const uri = props.uri;

    this.state = {
      uri,
      isLoading: true,
      hasError: false,
      imageRatio: 0,
    };
  }

  componentDidMount() {
    this._calculateImageSize();
  }

  componentWillUnmount() {
    console.log('UNMOUNTING');
  }

  _calculateImageSize() {
    Image.getSize(this.state.uri, (width = 1, height = 1) => {
      // get the image ratio and clip it if desired
      // clipping is done by a combo of restricting the view aspect ratio
      // and using image resizeMode of cover
      const imageRatio = this.props.shouldClipImage ?
        _.clamp(width/height, MIN_RATIO, MAX_RATIO)
        : width/height;
      this.setState({
        imageRatio,
        isLoading: false,
      });
    }, () => {
      console.log('Error in Image.getSize()');
      this.setState({
        hasError: true,
        isLoading: false,
      });
    });
  }

  render() {
    let pageContent;
    if (!this.state.isLoading && this.state.hasError) {
      pageContent = <View style={styles.imageOverlay}>
       <Text style={styles.overlayText}>Error loading image.</Text>
      </View>
    }
    else if (this.state.isLoading) {
      pageContent = <View style={styles.imageOverlay}>
        <Text style={styles.overlayText}>LOADING...</Text>
      </View>
    } else {
      pageContent = <Image style={[{aspectRatio: this.state.imageRatio},
         styles.image]}
        source={{uri: this.state.uri}} />
    }
    return (
      <TouchableWithoutFeedback style={{flex: 1}}
        onPress={this.props.onPressImage}>
        {pageContent}
      </TouchableWithoutFeedback>
    );

  }
}

ImageAutoSize.propTypes = {
  uri: PropTypes.string.isRequired,
  // When true, still keeps the image at the correct aspect ratio,
  // but the image will be clipped on the top and bottom if it has an
  // extreme aspect ratio.
  shouldClipImage: PropTypes.bool.isRequired,
  onPressImage: PropTypes.func.isRequired,
};

export default ImageAutoSize;

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
    backgroundColor: '#CCC',
  },
  imageOverlay: {
    backgroundColor: '#CCC',
    width: '100%',
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlayText: {
    fontSize: 24,
    color: '#FFF',
  },
});
